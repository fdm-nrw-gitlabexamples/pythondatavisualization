# FAIR Data Collection, Analysis, and Visualization
This repository contains an example of how the collection of research data can
be organized via GitLab. This comprises collecting data via Merge Requests,
validating data files against a schema, and automatically generating up-to-date
plots of all collected data sets.

As a running example for this use case, we use a data set of weight measurements
of coffee beans. This data set aims to answer one simple question: What is the
weight of a coffee bean? This is an open data set and we aim to extend it as
time progresses. If you would like to contribute to this data set, please refer
to the respective section below.

Please note that this project is a rather complex example and uses many
different moving parts, both of GitLab itself and in the employed software.
We try to explain all employed parts in this README. Should you have additional
questions, please feel free to [open an issue](https://gitlab.com/fdm-nrw-gitlabexamples/pythondatavisualization/-/issues/new)
in the issue tracker of this repository.


## Short Summary
This project was designed to collaboratively collect, aggregate, and visualize
the coffee weight data set. A contributor can create Merge Request to add a new
csv file with measurements or to propose a new visualization. To assure the
quality of both contributed data sets and contributed code, merge requests
must pass two tests before they can be merged:

- Data sets need to conform to a schema, e.g. to prevent negative weights and
  assure dates are formatted correctly.
- Python code needs to be formatted with an up-to-date version of the black
  code formatter.

Finally, when a merge request is then added to the main branch, a data analysis
pipeline is started which generates plots from the current data set. These are
then provided as pipeline artifacts:

- [Newest plots](https://gitlab.com/fdm-nrw-gitlabexamples/pythondatavisualization/-/jobs/artifacts/main/browse/figures?job=generate_plots)
- [All plots](https://gitlab.com/fdm-nrw-gitlabexamples/pythondatavisualization/-/pipelines?page=1&scope=finished&ref=main)

Plots can be downloaded by clicking on the three dots on the right of a successful job.


## Repository Content
This repository contains a tidy dataset of coffee bean weights across different
roasts, retailers, etc. The following files and folders are contained:

- `scripts`: This folder contains Python scripts used for validation, plotting, etc.
- `envs`: This folder contains environment files for conda environments,
- `raw_data`: This folder contains csv files that each describe a measurement of
  one roast of coffee. Details on the file format can be found below.
- `raw_data/data_dictionary.org`: This file describes the variables contained in the
  datasets. This file is a (plain text) emacs org-mode file.
- `raw_data/device_list.org`: Contains details of the scales used for measurements.
  This file is a (plain text) emacs org-mode file.
- `README.md`: The file you are reading right now.
- `Snakefile`: This file describes a snakemake workflow that merges all single
  csv files from the `raw_data` folder into one csv file and generates some plots
  from them.
- `.gitlab-ci.yml`: This file defines pipelines that are automatically executed
  by GitLab on certain conditions. These comprise validation of csv files in the
  `raw_data` folder, code linting for all Python scripts in the `scripts` folder,
  and plotting results after new data was successfully added.


## GitLab Features

### Protected Main Branch
In this repository, the main branch is [protected](https://docs.gitlab.com/ee/user/project/protected_branches.html).
This means that only project members (with the right privileges) are allowed
to push to this branch. Thus, in order to contribute to this project, users need
to create their own branches.

This feature is intended to prevent unchecked commits being added to the
main branch.


### Merge Requests to Add Measurements
In large collaborative projects it is common practice discuss and iterate on
proposed changes in Merge Requests (called Pull Requests on GitHub).
This allows to observe one chain of changes independently from other changes
occurring at the same time. The usual (and slightly simplified) approach for
this, from the view of a contributor, would be as follows:

1. The contributor creates a fork of this repository. This is a copy of the
   repository in which the contributor is allowed to author changes.
2. The contributor clones this repository or works with it through the web
   interface.
3. They create a new branch and add their changes to this branch.
4. Once they are done with their contribution, they go back to the main
   repository to open a Merge Request for their branch. There should be a
   message on the top of the project overview page.
5. In this Merge Request, maintainer and contributor can review the proposed
   changes.
6. Finally, the maintainer can merge the changes in the main repository.

Through this process, all changes to the main branch of the repository go
through an audit and, in most cases, have to pass automated tests.


### GitLab Runners and Pipelines
GitLab allows to run code when a new commit is pushed to the git repository.
This is facilitated through [pipelines](https://docs.gitlab.com/ee/ci/pipelines/index.html)
executed via [GitLab runners](https://docs.gitlab.com/runner/).
Strongly simplified, if you trigger a pipeline, a virtual machine is created and
code you specified in the file [`.gitlab-ci.yml`](.gitlab-ci.yml) is run on that VM.

This project defines three different pipelines.


#### Lint
The `lint_code` pipeline is run for all merge requests.
It install and runs the code linter and formatter `black` to ensure that the
Python code in the scripts folder is formatted to black's standards.


#### Dataset Schema
The `validate_data` pipeline is only executed on merge requests that modify the
`raw_data` folder. It creates and activates a conda environment containing the
required Python packages (defined in [`envs/validate_raw_data.yml`](envs/validate_raw_data.yml))
and executes the Python script [`scripts/dataset_validation.py`](scripts/dataset_validation.py).
This script contains a schema defining rules for the input datasets, e.g. that
a weight measurement of a coffee bean has to be a float between 0.0 and 1.0 grams.


#### Plotting
The pipeline `generate_plots` is only triggered by changes on the main branch,
e.g. from merging a MR that adds new data.
It runs a snakemake workflow (described in more detail below), that generates
plots from the current set of measurements. These plots are stored as so called
`artifacts` that can be downloaded from the `CI/CD → Pipelines` menu by clicking
on the three-point menu on the right of a successfully executed pipeline.

GitLab will always return the newest set of artifacts as well as previous
versions not older than 3 months (this time is specified using the `expire_in`
keyword in [`.gitlab-ci.yml`](.gitlab-ci.yml)).


## Snakemake Workflows
[Snakemake](https://github.com/snakemake/snakemake) is a workflow management
system used to define reproducible and scalable data analysis pipelines.
Note that snakemake is not a feature of GitLab, but since its workflows are
defined as text files using a super-set of Python, snakemake workflows can be
managed effectively with GitLab.
For an introduction on snakemake, please refer to snakemake's [documentation](https://snakemake.readthedocs.io/en/stable/).

Briefly, a snakemake workflow defines rules that each described how a set of
output files can be generated from a given set of input files.
Starting for a target rule, usually called `all` that defines a set of files
that should be generated, snakemake then runs all rules required to generate
these files. These rules are defined in the file [`Snakefile`](Snakefile).

In our example, we define the plots we want in the rule `all`. These can be
generated using the rule `plot_coffee_data` which requires files supplied by the
rule `aggregate_data_sets`.


## Data Set
Coffee measurements are collected as csv files in the folder `raw_data` with one
file per measurement.


### Raw Data Format
Data in the table is formatted as [Tidy Data](https://cran.r-project.org/web/packages/tidyr/vignettes/tidy-data.html)
i.e. each row is a measurement and values in each column contains values for
one variable. This format allows frictionless downstream analysis as can be seen
in the Python plotting script.

The precise definitions on which column should contain what data and in which
format can be found in the [data dictionary](raw_data/data_dictionary.org).

For new measurements, the file `raw_data/template/YYYY-MM-DD_Measurement-Template_Batch-X.csv` can be
used as a template. Alternatively, exporting a tidy data frame using the pandas
libraries `.to_csv` function should result in the same format.

### Naming Convention
Coffee measurement files follow the pattern:

    YYYY-MM-DD_<coffee-name>_Batch-<batch-nr>.csv
        │             │              │
        │             │              └── The batch number denotes how many times
        │             │                  this coffee has been measured already.
        │             │                  New coffees start at 0.
        │             └────── The name of the coffee, separated by dashes.
        └────── Date of measurement in ISO format.

For example a coffee with the name `"Foo Brew"` that already has been measured
twice and receives a third measurement on the 12th of October 2021 would receive
the file name `2021-10-12_ Foo-Brew_Batch-2.csv`.


### Measurement Protocol
Coffee beans were measured using a consumer grade precision scale with at least
two digits after the decimal point.
For each data point, 10 coffee beans were weighed and their average weight
reported in the data table. E.g. if you weigh 10 beans and the scale reads
a total weight of `1.42g` for those 10 beans, the entry in the table would be
`0.142` (`1.42g / 10`). No rounding is performed in the raw data.
For the precision levels and metadata of the employed scales refer to the file
`device_list.org`.

A measurement should contain about 50 data points, i.e. 50 average weights from
10 beans each. Only one measurement (i.e. one batch) should be submitted for
each bag of coffee.

## Running the Pipeline Locally
To generate the plots locally on you machine, you need to have the `conda`
package manager for Python installed. To execute the workflow you also need to
install `snakemake`.

This workflow was developed on Linux (specifically Ubuntu 20.04), but should
also run without issues on Mac OS. For Windows, probably the easiest way is
to install the [windows subsystem for linux](https://docs.microsoft.com/en-us/windows/wsl/about)(WSL)
which allows you to use a Linux system from within Windows.

For an installation guide for `conda`, please refer to the [conda website](https://docs.conda.io/projects/conda/en/latest/).
This workflow requires the [Python 3 version](https://docs.conda.io/en/latest/miniconda.html) of `(mini)conda`.

We recommend installing `snakemake` via the conda package manager using the bioconda channel to obtain a recent version.
```bash
conda install -c conda-forge -c bioconda snakemake mamba
```
Installing `mamba` is optional, but allows for faster creation of conda
environments which can speed up the execution.
This workflow was tested with `snakemake==6.8.2` on Ubuntu 20.04.

To run the pipeline, navigate to the folder containing the `Snakefile` and run:

    snakemake --cores 1 --use-conda

This will run the pipeline using one processor core and put the plots in a
folder named figures in the same directory.


## Contribution Guidelines
This repository does not only serve as an example of a finished project but also
for an ongoing distributed open science project (albeit with a simple example).
Please feel welcome to contribute code and measurements to this repository.
Depending on the kind of your contribution, please follow the guidelines below.

### Adding a Coffee Measurement
Currently, one of the strongest confounding factors to this dataset is the taste
in coffee of a sole person who has committed measurements.
If you would like to diversify this data set, please follow these steps:

1. Take a look into the data dictionary to find out which data points are required
2. Perform your measurement as described under "Measurement Protocol"
3. Save your measurements as a csv file (you can copy the template file for this)
4. Create a new branch in this repository and add the csv file to the `raw_data`
   folder and add yourself to the list of contributors.
5. Open a Merge Request (MR) for your branch and add the `Measurement` label to it.
6. Your MR needs to pass the automated data and code validation tests before
   it can be merged.

If you have any questions about this process, please feel free to open an issue
in the [issue tracker](https://gitlab.com/fdm-nrw-gitlabexamples/pythondatavisualization/-/issues) of this project.

### Adding or Modifying Visualization
Please feel free to add different visualizations or propose modifications to the
existing ones to this repository. The process to contribute code is similar to
adding a coffee measurement:

1. Create a new branch in this repository
2. Add your changes and assure that they work without error.
3. Format your Python code with the [black code formatter](https://github.com/psf/black)
4. Add yourself to the list of contributors.
5. Open a Merge Request (MR) for your branch and add the `Plotting` label to it.
6. Your MR needs to pass the automated code validation tests before it can be merged.

### Proposing Changes and Reporting Bugs
If you would like to propose an improvement or have spotted a bug, please open
an issue in the [issue tracker](https://gitlab.com/fdm-nrw-gitlabexamples/pythondatavisualization/-/issues) of this project.


## Contributors
The following people (Coffee Dataset Contributors) have contributed to this
repository either by improving the code or by adding measurements to the dataset:

- @HenningTimm


## License
The licensing of this repository is split between the dataset (comprising the
content of the `raw_data` folder, as well as all derived data files, plots,
and artifacts) and the source code (comprising the content of the `envs`
and `scripts` folders, as well as the files `.gitlabci.yaml`, `Snakefile`, and
this `README.md`).

The dataset is licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) as described in the file [`raw_data/LICENSE.dataset.txt`](raw_data/LICENSE.dataset.txt).

The source code is licensed under the MIT license as described in the file [`LICENSE`](LICENSE).

